#!/usr/bin/python

######################
# WARNING: UNTESTED! #
######################

from ansible.module_utils.basic import *


def _enable(module, extension):
    if module.check_mode:
        return True

    val = module.run_command(
        ["gnome-shell-extension-tool", "-e", extension])[1].strip()
    return (val == "'%s' is now enabled." % extension or
            val == "'%s' is already enabled." % extension)


def _disable(module, extension):
    if module.check_mode:
        return True

    val = module.run_command(
        ["gnome-shell-extension-tool", "-d", extension])[1].strip()
    return (val == "'%s' is now disabled." % extension or
            val == "'%s' is not enabled or installed." % extension)


def _list(module):
    extensions = module.run_command(
        ["dconf", "read", "/org/gnome/shell/enabled-extensions"])
    # Remove array braces
    extensions = extensions[1:-1].split(", ")
    # Remove quotes from values
    return [extension[1:-1] for extension in extensions]


def main():
    module = AnsibleModule(
        argument_spec={
            "extension": {
                "required": True,
                "type": "str"
            },
            "state": {
                "required": True,
                "type": "str",
                "choices": ["present", "absent"]
            },
        },
        supports_check_mode=True)

    extension = module.params["extension"]
    present = module.params["state"] == "present"

    changed = False
    failed = False

    currentExts = _list(module)
    if extension in currentExts:
        if not present:
            changed = True
            failed = not _enable(module, extension)
    else:
        if present:
            changed = True
            failed = not _disable(module, extension)

    module.exit_json(
        failed=failed,
        changed=changed,
        path=path,
        value=value,
        old_value=old_value)


main()
