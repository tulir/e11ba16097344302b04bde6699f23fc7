#!/usr/bin/python

from ansible.module_utils.basic import *


def _set_value(module, path, value):
    val = module.run_command(["dconf", "write", path, value])[1].strip()


def _get_value(module, path):
    return module.run_command(["dconf", "read", path])[1].strip()


def _format(typ, value):
    if typ == "bool":
        value = str(value).lower()
    elif typ == "int":
        value = str(int(value))
    elif typ == "float":
        value = str(float(value))
    elif typ == "string":
        value = "'%s'" % value
    elif typ == "array":
        arr = value
        value = ""
        for item in arr:
            value += "'%s', " % _format(item["type"], item["value"])
        value = "[%s]" % value[:-2]
    return value


def main():
    module = AnsibleModule(
        argument_spec={
            "path": {
                "required": True,
                "type": "str"
            },
            "type": {
                "type": "str",
                "required": False,
            },
            "value": {
                "required": True,
            },
        },
        supports_check_mode=True)

    path = module.params["path"]
    value = module.params["value"]

    old_value = _get_value(module, path)

    value = _format(module.params["type"], value)

    changed = old_value != str(value)
    failed = False

    if changed and not module.check_mode:
        out = _set_value(module, path, value)
        if type(out) == str and out.startswith("error: "):
            failed = True

    module.exit_json(
        failed=failed,
        changed=changed,
        path=path,
        value=value,
        old_value=old_value)


main()
